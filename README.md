# beHive Production

## License & Attribution

The beHive Show is available under the Creative Commons CC0 1.0 License, meaning you are free to use it for any purpose, commercial or non-commercial, without any attribution back to the original authors (public domain).
